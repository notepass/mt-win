﻿using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace mt_win
{
    internal class Program
    {
        private static readonly string DEFAULT_TAPE_DEVICE = Environment.GetEnvironmentVariable("TAPE") ?? @"\\.\TAPE0";
        private static string selectedTapeDevice = DEFAULT_TAPE_DEVICE;
        private static readonly string[] COMMANDS = {
            "weof",
            "wset",
            "eof",
            "fsf",
            "fsfm",
            "bsf",
            "bsfm",
            "fsr",
            "bsr",
            "fss",
            "bss",
            "rewind",
            "offline",
            "rewoffl",
            "eject",
            "retension", //TODO
            "eod",
            "seod",
            "seek",
            "tell",
            "status",
            "erase", //TODO
            "setblk",
            "lock",
            "unlock",
            "load",
            "compression",
            "asf",
            
            // ST (internal) driver options, I do not support these here
            "setdensity",
            "drvbuffer",
            "stwrthreshold",
            "stoptions",
            "stsetoptions",
            "stclearoptions",
            "defblksize",
            "defdensity",
            "defdrvbuffer",
            "defcompression",
            "stsetcln",
            "sttimeout",
            "stlongtimeout",
            "densities",
            "setpartition",
            "mkpartition",
            "partseek",
            "stshowoptions"
        };

        private static uint tapeDriveHandle = 0;

        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                PrintHelp();
                return;
            }

            if (args[0] == "-v" || args[0] == "--version")
            {
                PrintVersion();
                return;
            }

            if (args[0] == "-h")
            {
                PrintHelp();
                return;
            }

            int tapeSelectPosition = -1;
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-f" && args.Length > i + 1)
                {
                    tapeSelectPosition = i;
                    ProcessTapeDeviceSelection(args[i + 1]);
                }
            }
            
            var commandArgs = args;
            if (tapeSelectPosition >= 0)
            {
                // Remove non command parameters
                commandArgs = new string[commandArgs.Length - 2];
                int targetPos = 0;
                for (int i = 0; i < args.Length; i++)
                {
                    if (i != tapeSelectPosition && i != tapeSelectPosition + 1)
                    {
                        commandArgs[targetPos] = args[i];
                        targetPos++;
                    }
                }
            }

            if (commandArgs.Length > 2 || commandArgs.Length == 0)
            {
                //TODO: Print mt-st like error
                Console.Error.WriteLine("Too many or too few command args");
                return;
            }

            try
            {
                if (commandArgs.Length == 2)
                {
                    ProcessCommand(commandArgs[0], Convert.ToUInt32(commandArgs[1]));
                }
                else
                {
                    ProcessCommand(commandArgs[0], 0);
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine($"Error: {e.Message}");
                return;
            }
        }

        public static void PrintHelp()
        {
            Console.Out.WriteLine("usage: mt [-v] [--version] [-h] [ -f device ] command [ count ]");
            Console.Out.WriteLine("commands: weof, wset, eof, fsf, fsfm, bsf, bsfm, fsr, bsr, fss, bss, rewind,");
            Console.Out.WriteLine("          offline, rewoffl, eject, retension, eod, seod, seek, tell, status,");
            Console.Out.WriteLine("          setblk, lock, unlock, load, compression, asf, setpartition,");
            Console.Out.WriteLine("          mkpartition, partseek.");
        }

        public static void PrintVersion()
        {
            Console.Out.WriteLine($"mt-win v. {typeof(Program).Assembly.GetName().Version}");
            Console.Out.WriteLine("default tape device: '"+DEFAULT_TAPE_DEVICE+"'");
        }

        public static void ProcessTapeDeviceSelection(string tapeDevice)
        {
            selectedTapeDevice = tapeDevice ?? DEFAULT_TAPE_DEVICE;
        }

        public static void ProcessCommand(string commandName, uint count)
        {
            commandName = SelectCommand(commandName);

            if (commandName == null)
            {
                //TODO: Output error message
                return;
            }

            ProcessHandleCreation();

            // TODO: Partition support
            switch (commandName)
            {
                case "weof":
                case "eof":
                    ProcessWriteMarks(WindowsTapeApi.TAPE_FILEMARKS, count);
                    break;
                case "rewind":
                    ProcessSeek(WindowsTapeApi.TAPE_REWIND, 0);
                    break;
                case "bsf":
                    ProcessSeek(WindowsTapeApi.TAPE_FILEMARKS, (uint) (count * -1));
                    break;
                case "fsf":
                    ProcessSeek(WindowsTapeApi.TAPE_FILEMARKS, count);
                    break;
                case "asf":
                    ProcessCommand("rewind", 0);
                    ProcessCommand("fsf", count);
                    break;
                case "fsr":
                    ProcessSeek(WindowsTapeApi.TAPE_SPACE_RELATIVE_BLOCKS, count);
                    break;
                case "bsr":
                    ProcessSeek(WindowsTapeApi.TAPE_SPACE_RELATIVE_BLOCKS, (uint) (count * -1));
                    break;
                case "fss":
                    ProcessSeek(WindowsTapeApi.TAPE_SPACE_SETMARKS, count);
                    break;
                case "bss":
                    ProcessSeek(WindowsTapeApi.TAPE_SPACE_SETMARKS, (uint) (count * -1));
                    break;
                case "fsfm":
                    ProcessCommand("fsf", count);
                    ProcessCommand("bsr", 1);
                    break;
                case "bsfm":
                    ProcessCommand("bsf", count);
                    ProcessCommand("fsr", 1);
                    break;
                case "eod":
                case "seod":
                    ProcessSeek(WindowsTapeApi.TAPE_SPACE_END_OF_DATA, 0);
                    break;
                case "offline":
                case "rewoffl":
                case "eject":
                    ProcessInteract(WindowsTapeApi.TAPE_UNLOAD);
                    break;
                case "wset":
                    ProcessWriteMarks(WindowsTapeApi.TAPE_SETMARKS, count);
                    break;
                //case "erase":
                //    //TODO: Correct call?
                //    ProcessInteract(WindowsTapeApi.TAPE_FORMAT);
                //    break;
                case "load":
                    ProcessInteract(WindowsTapeApi.TAPE_LOAD);
                    break;
                case "lock":
                    ProcessInteract(WindowsTapeApi.TAPE_LOCK);
                    break;
                case "unlock":
                    ProcessInteract(WindowsTapeApi.TAPE_UNLOCK);
                    break;
                case "seek":
                    // TODO: How to handle partitions?
                    // TODO: How to handle absolute vs logical positioning? (Absolute probably only needed for non-LTO devices)
                    ProcessSeek(WindowsTapeApi.TAPE_LOGICAL_BLOCK, count);
                    break;
                case "tell":
                    ProcessTell();
                    break;
                case "compression":
                    ProcessSetTapeDriveConf(count != 0, null, null, null, null);
                    break;
                case "setblk":
                    ProcessSetTapeMediaConf(count);
                    break;
                case "status":
                    ProcessStatus();
                    break;
                
                case "stshowoptions":
                case "densities":
                case "stlongtimeout":
                case "sttimeout":
                case "stsetcln":
                case "defcompression":
                case "defdrvbuffer":
                case "defdensity":
                case "defblksize":
                case "stsetoptions":
                case "stclearoptions":
                case "stoptions":
                case "stwrthreshold":
                case "drvbuffer":
                case "setdensity":
                    Console.Error.WriteLine($"Command {commandName} is not supported on the windows platform");
                    break;
                
                default:
                    Console.Error.WriteLine("Command "+commandName+" is currently not implemented.");
                    break;
            }
        }

        public static void ProcessStatus()
        {
            WindowsTapeApi.TAPE_GET_DRIVE_PARAMETERS driveParams = new WindowsTapeApi.TAPE_GET_DRIVE_PARAMETERS(); 
            WindowsTapeApi.TAPE_GET_MEDIA_PARAMETERS mediaParams = new WindowsTapeApi.TAPE_GET_MEDIA_PARAMETERS();
            WindowsTapeApi.TAPE_GET_POSITION tapePositionAbs = new WindowsTapeApi.TAPE_GET_POSITION();
            tapePositionAbs.Type = WindowsTapeApi.TAPE_ABSOLUTE_POSITION;
            WindowsTapeApi.TAPE_GET_POSITION tapePositionRel = new WindowsTapeApi.TAPE_GET_POSITION();
            tapePositionRel.Type = WindowsTapeApi.TAPE_LOGICAL_POSITION;

            uint lowTemp;
            uint highTemp;
            
            uint structSize = 32;
            checkError(WindowsTapeApi.GetTapeParameters(tapeDriveHandle, WindowsTapeApi.GET_TAPE_MEDIA_INFORMATION, out structSize ,mediaParams));
            checkError(WindowsTapeApi.GetTapeParameters(tapeDriveHandle, WindowsTapeApi.GET_TAPE_DRIVE_INFORMATION, out structSize ,driveParams));
            checkError(WindowsTapeApi.GetTapePosition(tapeDriveHandle, tapePositionAbs.Type, out tapePositionAbs.Partition ,out lowTemp, out highTemp));
            tapePositionAbs.Offset = (ulong) lowTemp << 32 | (ulong) highTemp & 0xFFFFFFFFL;
            checkError(WindowsTapeApi.GetTapePosition(tapeDriveHandle, tapePositionRel.Type, out tapePositionRel.Partition ,out lowTemp, out highTemp));
            tapePositionRel.Offset = (ulong) lowTemp << 32 | (ulong) highTemp & 0xFFFFFFFFL;
            uint status = WindowsTapeApi.GetTapeStatus(tapeDriveHandle);
            bool mediaPresent = status != WindowsTapeApi.ERROR_NO_MEDIA_IN_DRIVE;
            
            Console.Out.WriteLine("======[ Device Information ]======");
            Console.Out.WriteLine($"Media present: {(mediaPresent ? "Yes" : "No")}");
            Console.Out.WriteLine($"Minimum Block size: {driveParams.MinimumBlockSize} / Maximum Block size: {driveParams.MaximumBlockSize} / Default Block size: {driveParams.DefaultBlockSize}");
            Console.Out.WriteLine($"Maximum Partition count: {driveParams.MaximumPartitionCount}");
            Console.Out.WriteLine("Enabled features (might be dependent on inserted media depending on tape drive type):");
            Console.Out.WriteLine($"  - ECC: {(driveParams.ECC != 0 ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Compression: {(driveParams.Compression != 0 ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Data padding: {(driveParams.DataPadding != 0 ? "Yes" : "No")}");
            Console.Out.WriteLine();
            Console.Out.WriteLine("Drive configuration:");
            Console.Out.WriteLine($"  - Report setmarks/filemarks: {(driveParams.ReportSetmarks != 0 ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - EOT warning zone in bytes: {driveParams.EOTWarningZoneSize}");
            Console.Out.WriteLine();
            
            Console.Out.WriteLine("Drive capabilities/features:");
            Console.Out.WriteLine($"  - Compression support: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_COMPRESSION) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - ECC support: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_ECC) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Custom end of media warning zone size support: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_SET_EOT_WZ_SIZE) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Variable block size support (Block size 0): {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_VARIABLE_BLOCK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Fixed-size block support: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_FIXED_BLOCK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Data padding support: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_PADDING) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Can report if cleaning is needed: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_CLEAN_REQUESTS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Device can return absolute block position: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_GET_ABSOLUTE_BLK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Device can return partition-relative block position and current partition index: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_GET_LOGICAL_BLK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Support setmark reporting: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_REPORT_SMKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Device can return overall media size: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_TAPE_CAPACITY) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Device can return remaining media size: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_TAPE_REMAINING) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Device honours write protection on media: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_WRITE_PROTECT) ? "Yes" : "No")}");
            
            Console.Out.WriteLine($"  - Ejects physical tape upon software request (hard unload): {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_EJECT_MEDIA) ? "Yes" : "No")}");

            Console.Out.WriteLine($"  - Erase operations are started from the partition start marker only: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_ERASE_BOP_ONLY) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Can Perform a long erase operation (Overwriting whole tape): {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_ERASE_LONG) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Can Perform a short erase operation (Setting EOT marker to first block): {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_ERASE_SHORT) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - Returns after initializing erase operation, not when finished: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_ERASE_IMMEDIATE) ? "Yes" : "No")}");

            Console.Out.WriteLine($"  - The device creates fixed data partitions: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_FIXED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - The device creates initiator-defined partitions: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_INITIATOR) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - The device creates select data partitions: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_SELECT) ? "Yes" : "No")}");
            
            Console.Out.WriteLine($"  - Compression can only be set on the beginning of a partition: {(HasFlag(driveParams.FeaturesLow, WindowsTapeApi.TAPE_DRIVE_SET_CMP_BOP_ONLY) ? "Yes" : "No")}");

            //TODO: Readable labels, based on https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-tape_get_drive_parameters but better
            Console.Out.WriteLine($"  - TAPE_DRIVE_ABS_BLK_IMMED: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_ABS_BLK_IMMED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_ABSOLUTE_BLK: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_ABSOLUTE_BLK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_END_OF_DATA: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_END_OF_DATA) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_FILEMARKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_FILEMARKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_LOAD_UNLOAD: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_LOAD_UNLOAD) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_LOAD_UNLD_IMMED: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_LOAD_UNLD_IMMED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_LOCK_UNLOCK: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_LOCK_UNLOCK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_LOCK_UNLK_IMMED: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_LOCK_UNLK_IMMED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_LOG_BLK_IMMED: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_LOG_BLK_IMMED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_LOGICAL_BLK: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_LOGICAL_BLK) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_RELATIVE_BLKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_RELATIVE_BLKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_REVERSE_POSITION: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_REVERSE_POSITION) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_REWIND_IMMEDIATE: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_REWIND_IMMEDIATE) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SEQUENTIAL_FMKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SEQUENTIAL_FMKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SEQUENTIAL_SMKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SEQUENTIAL_SMKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SET_BLOCK_SIZE: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SET_BLOCK_SIZE) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SET_COMPRESSION: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SET_COMPRESSION) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SET_ECC: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SET_ECC) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SET_PADDING: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SET_PADDING) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SET_REPORT_SMKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SET_REPORT_SMKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SETMARKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SETMARKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_SPACE_IMMEDIATE: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_SPACE_IMMEDIATE) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_TENSION: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_TENSION) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_TENSION_IMMED: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_TENSION_IMMED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_WRITE_FILEMARKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_WRITE_FILEMARKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_WRITE_LONG_FMKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_WRITE_LONG_FMKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_WRITE_MARK_IMMED: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_WRITE_MARK_IMMED) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_WRITE_SETMARKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_WRITE_SETMARKS) ? "Yes" : "No")}");
            Console.Out.WriteLine($"  - TAPE_DRIVE_WRITE_SHORT_FMKS: {(HasFlag(driveParams.FeaturesHigh, WindowsTapeApi.TAPE_DRIVE_WRITE_SHORT_FMKS) ? "Yes" : "No")}");
            
            Console.Out.WriteLine();
            Console.Out.WriteLine("======[ Media Information ]======");
            if (mediaPresent)
            {
                Console.Out.WriteLine($"Overall Partitions: {mediaParams.PartitionCount} / Current Partition: {tapePositionRel.Partition}");
                Console.Out.WriteLine($"Relative Position in Partition: {tapePositionRel.Offset} / Absolute Position on Tape: {tapePositionAbs.Offset}");
                Console.Out.WriteLine($"Relative Position in Partition: {tapePositionRel.Offset} / Absolute Position on Tape: {tapePositionAbs.Offset}");
                Console.Out.WriteLine($"Current Block size: {mediaParams.BlockSize} / Overall bytes available: {mediaParams.Capacity} / Remaining bytes: {mediaParams.Remaining}");
                Console.Out.WriteLine($"Write protected: {(mediaParams.WriteProtected ? "Yes" : "No")}");
            }
            else
            {
                Console.Out.WriteLine("Skipping media information: No media inserted");
            }
        }

        public static bool HasFlag(uint data, uint flagMask)
        {
            return (data & flagMask) != 0;
        }

        public static void ProcessHandleCreation()
        {
            tapeDriveHandle = WindowsTapeApi.CreateFile(
                selectedTapeDevice, 
                WindowsTapeApi.GENERIC_READ | WindowsTapeApi.GENERIC_WRITE, 
                0, 
                null, 
                WindowsTapeApi.OPEN_EXISTING, 
                0, 
                0
            );
            var error = Marshal.GetLastWin32Error();

            if (tapeDriveHandle == uint.MaxValue)
            {
                // Could not get file handle
                throw new Exception(
                    $"Could not create file handle for tape device {selectedTapeDevice} (Error code {error})");
            }
        }

        public static void ProcessWriteMarks(uint type, uint count)
        {
            if (count < 1)
            {
                //TODO: print error
                return;
            }

            WindowsTapeApi.WriteTapemark(tapeDriveHandle, type, count, false);
        }

        public static void ProcessTell()
        {
            // TODO: Check output with mt-st
            Console.Out.WriteLine($"At block {GetTapeBlockPosition()}.");
        }

        
        public static ulong GetTapeBlockPosition(bool useLogical = true)
        {
            uint part = 0;
            uint low = 0;
            uint high = 0;

            uint op = WindowsTapeApi.TAPE_ABSOLUTE_POSITION;
            if (useLogical)
            {
                op = WindowsTapeApi.TAPE_LOGICAL_POSITION;
            }
            WindowsTapeApi.GetTapePosition(tapeDriveHandle, op, out part, out low,
                out high);
            
            // TODO: Wrong shift direction?
            return ((ulong)low) | (((ulong)high) >> 32);
        }

        public static void ProcessSeek(uint type, uint partition, ulong offset)
        {
            uint offsetLow = (uint) offset;
            uint offsetHigh = (uint) (offset >> 32);

            WindowsTapeApi.SetTapePosition(tapeDriveHandle, type, partition, offsetLow, offsetHigh, false);
        }
        
        public static void ProcessSeek(uint type, UInt64 offset)
        {
            ProcessSeek(type, 0, offset);
        }

        public static void ProcessInteract(uint operation)
        {
            WindowsTapeApi.PrepareTape(tapeDriveHandle, operation, false);
        }

        public static void ProcessSetTapeDriveConf(bool? compression, bool? dataPadding, bool? reportSetmakrs, bool? ecc, uint? eotWarningZoneSize)
        {
            WindowsTapeApi.TAPE_SET_DRIVE_PARAMETERS info = new WindowsTapeApi.TAPE_SET_DRIVE_PARAMETERS();
            if (compression != null)
            {
                if (compression == true)
                {
                    info.Compression = 0xFF;
                }
                else
                {
                    info.Compression = 0x00;
                }
            }

            if (dataPadding != null)
            {
                if (dataPadding == true)
                {
                    info.DataPadding = 0xFF;
                }
                else
                {
                    info.DataPadding = 0x00;
                }
            }
            
            if (reportSetmakrs != null)
            {
                if (reportSetmakrs == true)
                {
                    info.ReportSetmarks = 0xFF;
                }
                else
                {
                    info.ReportSetmarks = 0x00;
                }
            }
            
            if (ecc != null)
            {
                if (ecc == true)
                {
                    info.Compression = 0xFF;
                }
                else
                {
                    info.Compression = 0x00;
                }
            }

            if (eotWarningZoneSize != null)
            {
                info.EOTWarningZoneSize = (uint) eotWarningZoneSize;
            }

            WindowsTapeApi.SetTapeParameters(tapeDriveHandle, WindowsTapeApi.SET_TAPE_DRIVE_INFORMATION, info);
        }

        public static void checkError(uint errorCode)
        {
            if (errorCode != 0)
            {
                throw new Exception(errorCode.ToString());
            }
        }
        
        public static void checkError(bool checkFlag)
        {
            if (!checkFlag)
            {
                checkError((uint)Marshal.GetLastWin32Error());
            }
        }

        public static bool IsFilemarkEncounteredError(Exception e)
        {
            string msg = e.Message;
            try
            {
                uint code = Convert.ToUInt32(msg);
                return IsFilemarkEncounteredError(code);
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public static bool IsFilemarkEncounteredError(uint errorCode)
        {
            return errorCode == WindowsTapeApi.ERROR_FILEMARK_DETECTED;
        }
        
        public static bool IsEndOfTapeError(Exception e)
        {
            string msg = e.Message;
            try
            {
                uint code = Convert.ToUInt32(msg);
                return IsEndOfTapeError(code);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsEndOfTapeError(uint errorCode)
        {
            return errorCode == WindowsTapeApi.ERROR_END_OF_MEDIA;
        }
        
        public static void ProcessSetTapeMediaConf(uint blockSize)
        {
            WindowsTapeApi.TAPE_SET_MEDIA_PARAMETERS info = new WindowsTapeApi.TAPE_SET_MEDIA_PARAMETERS();
            info.BlockSize = blockSize;
            WindowsTapeApi.SetTapeParameters(tapeDriveHandle, WindowsTapeApi.SET_TAPE_MEDIA_INFORMATION, info);
        }

        /**
         * mt-st allows to use shorthands for commands (e.g. stat instead of status)<br/>
         * This is completely dynamic (e.g. sta, stat, statu and status will map to status).
         * This function tries to mimic that behaviour.<br/>
         * Returns the value found or null if no or multiple values where found matching
         */
        public static string SelectCommand(string commandPart)
        {
            if (COMMANDS.Contains(commandPart))
            {
                // The given command is the full command
                return commandPart;
            }


            string match = null;
            int numMatches = 0;
            foreach (string command in COMMANDS)
            {
                if (command.StartsWith(commandPart))
                {
                    match = command;
                    numMatches++;
                }
            }

            return numMatches == 1 ? match : null;
        }
    }
}