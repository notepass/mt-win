﻿using System;
using System.Runtime.InteropServices;

namespace mt_win
{
    // TODO: Make this a lib between mt-win and mt-cp
    public class WindowsTapeApi
    {
        
        /// ============================== HANDLES ============================== ///
        
        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        
        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;
        public const uint OPEN_ALWAYS = 4;
        public const uint TRUNCATE_EXISTING = 5;
        
        [StructLayout(LayoutKind.Sequential)]
        public class SecurityAttributes
        {
            public uint size;
            public uint securityDescriptor;
            public uint inheritHandle;
        }
        
        [DllImport("kernel32.dll")]
        public static extern uint CreateFile(
            string lpFileName,
            uint dwDesiredAccess,
            uint dwShareMode,
            SecurityAttributes lpSecurityAttributes,
            uint dwCreationDisposition,
            uint dwFlagsAndAttributes,
            uint hTemplateFile
        );
        
        
        /// ============================== TAPEMARKS ============================== ///
        
        
        /**  Writes the number of setmarks specified by dwTapemarkCount.  */
        public const uint TAPE_SETMARKS = 1;
        /**  Writes the number of filemarks specified by the dwTapemarkCount parameter.  */
        public const uint TAPE_FILEMARKS = 1;
        /**  Writes the number of short filemarks specified by dwTapemarkCount.  */
        public const uint TAPE_SHORT_FILEMARKS = 1;
        /**  Writes the number of long filemarks specified by dwTapemarkCount.  */
        public const uint TAPE_LONG_FILEMARKS = 1;
        
        /**
         * The WriteTapemark function writes a specified number of filemarks, setmarks, short filemarks,
         * or long filemarks to a tape device. These tapemarks divide a tape partition into smaller areas.
         */
        [DllImport("kernel32.dll")]
        public static extern uint WriteTapemark(
            uint hDevice,
            uint dwTapemarkType,
            uint dwTapemarkCount,
            bool bImmediate
        );


        /// ============================== POSITIONING ============================== ///


        public const uint TAPE_REWIND = 0;
        public const uint TAPE_ABSOLUTE_BLOCK = 1;
        public const uint TAPE_LOGICAL_BLOCK = 2;
        public const uint TAPE_SPACE_END_OF_DATA = 4;
        public const uint TAPE_SPACE_RELATIVE_BLOCKS = 5;
        public const uint TAPE_SPACE_FILEMARKS = 6;
        public const uint TAPE_SPACE_SEQUENTIAL_FMKS = 7;
        public const uint TAPE_SPACE_SETMARKS = 8;
        public const uint TAPE_SPACE_SEQUENTIAL_SMKS = 9;

        [DllImport("kernel32.dll")]
        public static extern uint SetTapePosition(
            uint hDevice,
            uint dwPositionMethod,
            uint dwPartition,
            uint dwOffsetLow,
            uint dwOffsetHigh,
            bool bImmediate
        );

        public const uint TAPE_ABSOLUTE_POSITION = 0;
        public const uint TAPE_LOGICAL_POSITION = 1;
        
        [DllImport("kernel32.dll")]
        public static extern uint GetTapePosition(
            uint hDevice,
            uint dwPositionType,
            out uint lpdwPartition,
            out uint lpdwOffsetLow,
            out uint lpdwOffsetHigh
        );


        /// ============================== TAPE INTERACTION ============================== ///

        // Capabilities list
        // Low bytes
        public const uint TAPE_DRIVE_COMPRESSION         = 0x00020000;
        public const uint TAPE_DRIVE_CLEAN_REQUESTS      = 0x02000000;
        public const uint TAPE_DRIVE_ECC                 = 0x00010000;
        public const uint TAPE_DRIVE_EJECT_MEDIA         = 0x01000000;
        public const uint TAPE_DRIVE_ERASE_BOP_ONLY      = 0x00000040;
        public const uint TAPE_DRIVE_ERASE_LONG          = 0x00000020;
        public const uint TAPE_DRIVE_ERASE_IMMEDIATE     = 0x00000080;
        public const uint TAPE_DRIVE_ERASE_SHORT         = 0x00000010;
        public const uint TAPE_DRIVE_FIXED               = 0x00000001;
        public const uint TAPE_DRIVE_FIXED_BLOCK         = 0x00000400;
        public const uint TAPE_DRIVE_GET_ABSOLUTE_BLK    = 0x00100000;
        public const uint TAPE_DRIVE_GET_LOGICAL_BLK     = 0x00200000;
        public const uint TAPE_DRIVE_INITIATOR           = 0x00000004;
        public const uint TAPE_DRIVE_PADDING             = 0x00040000;
        public const uint TAPE_DRIVE_REPORT_SMKS         = 0x00080000;
        public const uint TAPE_DRIVE_SELECT              = 0x00000002;
        public const uint TAPE_DRIVE_SET_CMP_BOP_ONLY    = 0x04000000;
        public const uint TAPE_DRIVE_SET_EOT_WZ_SIZE     = 0x00400000;
        public const uint TAPE_DRIVE_TAPE_CAPACITY       = 0x00000100;
        public const uint TAPE_DRIVE_TAPE_REMAINING      = 0x00000200;
        public const uint TAPE_DRIVE_VARIABLE_BLOCK      = 0x00000800;
        public const uint TAPE_DRIVE_WRITE_PROTECT       = 0x00001000;
        
        // High bytes
        public const uint TAPE_DRIVE_ABS_BLK_IMMED       = 0x80002000;
        public const uint TAPE_DRIVE_ABSOLUTE_BLK        = 0x80001000;
        public const uint TAPE_DRIVE_END_OF_DATA         = 0x80010000;
        public const uint TAPE_DRIVE_FILEMARKS           = 0x80040000;
        public const uint TAPE_DRIVE_LOAD_UNLOAD         = 0x80000001;
        public const uint TAPE_DRIVE_LOAD_UNLD_IMMED     = 0x80000020;
        public const uint TAPE_DRIVE_LOCK_UNLOCK         = 0x80000004;
        public const uint TAPE_DRIVE_LOCK_UNLK_IMMED     = 0x80000080;
        public const uint TAPE_DRIVE_LOG_BLK_IMMED       = 0x80008000;
        public const uint TAPE_DRIVE_LOGICAL_BLK         = 0x80004000;
        public const uint TAPE_DRIVE_RELATIVE_BLKS       = 0x80020000;
        public const uint TAPE_DRIVE_REVERSE_POSITION    = 0x80400000;
        public const uint TAPE_DRIVE_REWIND_IMMEDIATE    = 0x80000008;
        public const uint TAPE_DRIVE_SEQUENTIAL_FMKS     = 0x80080000;
        public const uint TAPE_DRIVE_SEQUENTIAL_SMKS     = 0x80200000;
        public const uint TAPE_DRIVE_SET_BLOCK_SIZE      = 0x80000010;
        public const uint TAPE_DRIVE_SET_COMPRESSION     = 0x80000200;
        public const uint TAPE_DRIVE_SET_ECC             = 0x80000100;
        public const uint TAPE_DRIVE_SET_PADDING         = 0x80000400;
        public const uint TAPE_DRIVE_SET_REPORT_SMKS     = 0x80000800;
        public const uint TAPE_DRIVE_SETMARKS            = 0x80100000;
        public const uint TAPE_DRIVE_SPACE_IMMEDIATE     = 0x80800000;
        public const uint TAPE_DRIVE_TENSION             = 0x80000002;
        public const uint TAPE_DRIVE_TENSION_IMMED       = 0x80000040;
        public const uint TAPE_DRIVE_WRITE_FILEMARKS     = 0x82000000;
        public const uint TAPE_DRIVE_WRITE_LONG_FMKS     = 0x88000000;
        public const uint TAPE_DRIVE_WRITE_MARK_IMMED    = 0x90000000;
        public const uint TAPE_DRIVE_WRITE_SETMARKS      = 0x81000000;
        public const uint TAPE_DRIVE_WRITE_SHORT_FMKS    = 0x84000000;
        
        
        // Functions

        public const uint TAPE_LOAD = 0;
        public const uint TAPE_UNLOAD = 1;
        public const uint TAPE_TENSION = 2;
        public const uint TAPE_LOCK = 3;
        public const uint TAPE_UNLOCK = 4;
        public const uint TAPE_FORMAT = 5;
        
        [DllImport("kernel32.dll")]
        public static extern uint PrepareTape(
            uint hDevice,
            uint dwOperation,
            bool bImmediate
        );
        
        [StructLayout(LayoutKind.Sequential)]
        public class TAPE_SET_DRIVE_PARAMETERS
        {
            public uint? ECC = null; 
            public uint? Compression = null; 
            public uint? DataPadding = null; 
            public uint? ReportSetmarks = null; 
            public uint? EOTWarningZoneSize = null;
        }
        
        [StructLayout(LayoutKind.Sequential)]
        public class TAPE_SET_MEDIA_PARAMETERS
        {
            public uint? BlockSize = null; 
        }

        public const uint SET_TAPE_MEDIA_INFORMATION = 0;
        public const uint SET_TAPE_DRIVE_INFORMATION = 1;

        [DllImport("kernel32.dll")]
        public static extern uint SetTapeParameters(
            uint hDevice,
            uint dwOperation,
            TAPE_SET_DRIVE_PARAMETERS lpTapeInformation
        );
        
        [DllImport("kernel32.dll")]
        public static extern uint SetTapeParameters(
            uint hDevice,
            uint dwOperation,
            TAPE_SET_MEDIA_PARAMETERS lpTapeInformation
        );
        
        [StructLayout(LayoutKind.Sequential)]
        public class TAPE_GET_MEDIA_PARAMETERS
        {
            public ulong Capacity;
            public ulong Remaining;
            public uint BlockSize;
            public uint PartitionCount;
            public bool WriteProtected;
        }
        
        [StructLayout(LayoutKind.Sequential)]
        public class TAPE_GET_POSITION
        {
            public uint Type;
            public uint Partition;
            public ulong Offset;
        }
        
        [StructLayout(LayoutKind.Sequential)]
        public class TAPE_GET_DRIVE_PARAMETERS
        {
            public uint ECC; 
            public uint Compression; 
            public uint DataPadding; 
            public uint ReportSetmarks; 
            public uint DefaultBlockSize; 
            public uint MaximumBlockSize; 
            public uint MinimumBlockSize; 
            public uint MaximumPartitionCount; 
            public uint FeaturesLow; 
            public uint FeaturesHigh; 
            public uint EOTWarningZoneSize; 
        }
        
        public const uint GET_TAPE_MEDIA_INFORMATION = 0;
        public const uint GET_TAPE_DRIVE_INFORMATION = 1;
        
        [DllImport("kernel32.dll")]
        public static extern uint GetTapeParameters(
            uint hDevice,
            uint dwOperation,
            out uint lpdwSize,
            TAPE_GET_MEDIA_PARAMETERS lpTapeInformation
        );
        
        [DllImport("kernel32.dll")]
        public static extern uint GetTapeParameters(
            uint hDevice,
            uint dwOperation,
            out uint lpdwSize,
            TAPE_GET_DRIVE_PARAMETERS lpTapeInformation
        );
        
        public const uint ERROR_BEGINNING_OF_MEDIA = 1102;
        public const uint ERROR_BUS_RESET = 1111;
        public const uint ERROR_DEVICE_NOT_PARTITIONED = 1107;
        public const uint ERROR_DEVICE_REQUIRES_CLEANING = 1165;
        public const uint ERROR_END_OF_MEDIA = 1100;
        public const uint ERROR_FILEMARK_DETECTED = 1101;
        public const uint ERROR_INVALID_BLOCK_LENGTH = 1106;
        public const uint ERROR_MEDIA_CHANGED = 1110;
        public const uint ERROR_NO_DATA_DETECTED = 1104;
        public const uint ERROR_NO_MEDIA_IN_DRIVE = 1112;
        public const uint ERROR_NOT_SUPPORTED = 50;
        public const uint ERROR_PARTITION_FAILURE = 1105;
        public const uint ERROR_SETMARK_DETECTED = 1103;
        public const uint ERROR_UNABLE_TO_LOCK_MEDIA = 1108;
        public const uint ERROR_UNABLE_TO_UNLOAD_MEDIA = 1109;
        public const uint ERROR_WRITE_PROTECT = 15;
        
        [DllImport("kernel32.dll")]
        public static extern uint GetTapeStatus(uint tapeHandle);
    }
}