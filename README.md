# mt-win
mt-win is a tool designed after mt-st on linux, which allows to execute basic operations on tape devices 
(e.g. seeking, writing filemarks and accessing information about the inserted tape cartridge).  
I'm trying to keep to output and commands as close as possible to mt-st, so that it can be used as a "drop in replacement".  
Please note that not all commands will be implemented, as mt-st has some options related to the st linux driver which cannot
be ported to windows.  
This tool is written in C# (.NET 4.0 and above) and only intended for use on Windows based systems with support for .NET 4.0 or newer.  
As the tape APIs haven't changed since Win XP according to the Microsoft docs, this should mean that this tool should be compatible
with Windows XP and newer (32 + 64 bit), although it has only been tested on Windows 10.

# Project state
Currently, the project is not production ready, but the most commands which can be implemented do something (expect for erase).  
The status command does quite a different output from the linux version, as the windows API returns other data than the linux
one.  
The status command also still needs some work, as fetching device/media information is still buggy.  
Additionally, partition seeking is not completely implemented. This can be problematic on LTO-5+ devices. LTO-4 and before
do not support partition, so they do not care.